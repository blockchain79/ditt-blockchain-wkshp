//Import the SubjectList smart contract
const SubjectList = artifacts.require('SubjectList')

//Use the contract  to write all tests
//variable: account => all accounts in blockchain
contract('SubjectList', (accounts) => {
  //Make sure contract is deployed and before
  //we retrieve the subjectlist object for testing
  beforeEach(async () => {
    this.subjectList = await SubjectList.deployed()
  })

  //Testing the deployed subject contract
  it('deploys successfully', async () => {
    //Get the address which the subject object is stored
    const address = await this.subjectList.address
    //Test for valid address
    isValidAddress(address)
  })
  //Testing the content in the contract
  it('test adding subjects', async () => {
    return SubjectList.deployed().then(async (instance) => {
      s = instance;
      subjectcode = "CSF101";
      return s.createSubject(subjectcode, "Front End Development")
        .then(async (transaction) => {
        isValidAddress(transaction.tx)
        isValidAddress(transaction.receipt.blockHash);
        return s.subjectsCount().then(async (count) => {
          assert.equal(count, 1)
          return s.subjects(1).then(async (subject) => {
            assert.equal(subject.code, subjectcode)
            return
          })
        })
      })
    })
  })
it('test finding subjects', async () => {
  return SubjectList.deployed().then((instance) => {
s = instance;
return s.createSubject("CSF102", "Fundamentals of Computing").then(() => {
return s.createSubject("CSF103", "Fundamentals of Programming").then(() => {
return s.createSubject("CSF104", "Modern Database Design").then(() => {
return s.createSubject("CSF105", "Back End Development").then(() => {
return s.createSubject("CSF106", "Mathematics for Programming I").then(() => {
return s.createSubject("CSF107", "Data Structures and Algorithms").then(() => {
return s.subjectsCount().then(async (count) => {
  assert.equal(count, 7)

  return s.findSubject(1).then((subject) => {
    assert.equal(subject.name, "Front End Development")
  })
})
})
})
})
})
})
})
})
})
  it('test mark retired subjects', async () => {
    return SubjectList.deployed().then((instance) => {
      s = instance;
      return s.findSubject(1).then((subject) => {
        assert.equal(subject.name, "Front End Development")
        assert.equal(subject.retired, false)
        return s.markRetired(1).then((subject) => {
          return s.findSubject(1).then((subject) => {
            assert.equal(subject.name, "Front End Development")
            assert.equal(subject.retired, true)
          })
        })
      })
    })
  })
})

function isValidAddress(address) {
  assert.notEqual(address, 0x0)
  assert.notEqual(address, '')
  assert.notEqual(address, null)
  assert.notEqual(address, undefined)
}