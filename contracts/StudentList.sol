// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

contract StudentList {
    // Store Students Count
    uint public studentsCount = 0;
    // Model a Student
    struct Student {
        uint _id;
        uint cid;
        string name;
        bool graduated;
    }
    // Store Students
    mapping(uint => Student) public students;
    // Events
    event createStudentEvent (
        uint _id,
        uint indexed cid,
        string name,
        bool graduated
    );
    event markGraduatedEvent (
        uint indexed cid
    );
    //Constructor for students
    constructor() {
        // createStudent( 10001, "Sonam Wangmo");
    }

    //Create and add student to storage
    function createStudent(uint _studentCid, string memory _name) 
        public returns (Student memory){
        studentsCount++;
        students[studentsCount] = Student(studentsCount,_studentCid, _name, false);
        // trigger create event
        emit createStudentEvent(studentsCount,_studentCid, _name, false);
        return students[studentsCount];
    }
    //Change graduation status of student
    function markGraduated(uint _id) 
        public returns (Student memory)
    {
        students[_id].graduated = true;
        // trigger create event
        emit markGraduatedEvent(_id);
        return students[_id];
    }
    //Fetch student info from storage
    function findStudent(uint _id)
        public view returns (Student memory)
    {
        return students[_id];
    }
}
