// export const MARKS_LIST_ADDRESS = '0x786Dc6D4e81C5a3705A734E3ca6F9679b9b61356' //OLD
export const MARKS_LIST_ADDRESS = '0x9D43e1Eb06A9190666f9448D13CcaeB4eA8E799E'//ROPSTEN
 
export const MARKS_LIST_ABI = [
  {
    "inputs": [],
    "stateMutability": "nonpayable",
    "type": "constructor"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "indexed": true,
        "internalType": "uint256",
        "name": "cid",
        "type": "uint256"
      },
      {
        "indexed": true,
        "internalType": "string",
        "name": "code",
        "type": "string"
      },
      {
        "indexed": false,
        "internalType": "enum MarksList.Grades",
        "name": "grades",
        "type": "uint8"
      }
    ],
    "name": "addMarksEvent",
    "type": "event"
  },
  {
    "inputs": [
      {
        "internalType": "uint256",
        "name": "",
        "type": "uint256"
      },
      {
        "internalType": "string",
        "name": "",
        "type": "string"
      }
    ],
    "name": "marks",
    "outputs": [
      {
        "internalType": "uint256",
        "name": "cid",
        "type": "uint256"
      },
      {
        "internalType": "string",
        "name": "code",
        "type": "string"
      },
      {
        "internalType": "enum MarksList.Grades",
        "name": "grades",
        "type": "uint8"
      }
    ],
    "stateMutability": "view",
    "type": "function",
    "constant": true
  },
  {
    "inputs": [],
    "name": "marksCount",
    "outputs": [
      {
        "internalType": "uint256",
        "name": "",
        "type": "uint256"
      }
    ],
    "stateMutability": "view",
    "type": "function",
    "constant": true
  },
  {
    "inputs": [
      {
        "internalType": "uint256",
        "name": "_cid",
        "type": "uint256"
      },
      {
        "internalType": "string",
        "name": "_code",
        "type": "string"
      },
      {
        "internalType": "enum MarksList.Grades",
        "name": "_grade",
        "type": "uint8"
      }
    ],
    "name": "addMarks",
    "outputs": [],
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "inputs": [
      {
        "internalType": "uint256",
        "name": "_cid",
        "type": "uint256"
      },
      {
        "internalType": "string",
        "name": "_code",
        "type": "string"
      }
    ],
    "name": "findMarks",
    "outputs": [
      {
        "components": [
          {
            "internalType": "uint256",
            "name": "cid",
            "type": "uint256"
          },
          {
            "internalType": "string",
            "name": "code",
            "type": "string"
          },
          {
            "internalType": "enum MarksList.Grades",
            "name": "grades",
            "type": "uint8"
          }
        ],
        "internalType": "struct MarksList.Marks",
        "name": "",
        "type": "tuple"
      }
    ],
    "stateMutability": "view",
    "type": "function",
    "constant": true
  }
]