//Step 2: change the base code to use Component
import React, { Component } from 'react'
import Web3 from 'web3'
import './App.css';
//Step 3 : Import the ABI
import { STUDENT_LIST_ABI, STUDENT_LIST_ADDRESS } from './abi/config_studentList'
import { SUBJECT_LIST_ABI, SUBJECT_LIST_ADDRESS } from './abi/config_subjectList'
import { MARKS_LIST_ABI, MARKS_LIST_ADDRESS } from './abi/config_marksList'
//Import the components
import StudentList from './components/StudentList'
import SubjectList from './components/SubjectList'
import MarksList from './components/MarksList'
import FindMarks from './components/FindMarks'
//We replace function with a component for better management of code
class App extends Component {
  //componentDidMount called once on the client after the initial render
  //when the client receives data from the server and before the data is displayed.
  componentDidMount() {
    if (!window.ethereum)
      throw new Error("No crypto wallet found. Please install it.");
    window.ethereum.send("eth_requestAccounts");
    this.loadBlockchainData()
  }
  //create a web3 connection to using a provider, MetaMask to connect to
  //the ganache server and retrieve the first account
  async loadBlockchainData() {
    // const web3 = new Web3(Web3.givenProvider || "http://localhost:7545")
    const web3 = new Web3(Web3.givenProvider ||
      'https://ropsten.infura.io/v3/70bbf2a1845f4ea795752765485e6468'
    )
    const accounts = await web3.eth.getAccounts()
    this.setState({ account: accounts[0] })
    //Step 4: load all the lists from the blockchain
    const studentList = new web3.eth.Contract(STUDENT_LIST_ABI, STUDENT_LIST_ADDRESS)
    const subjectList = new web3.eth.Contract(SUBJECT_LIST_ABI, SUBJECT_LIST_ADDRESS)
    const marksList = new web3.eth.Contract(MARKS_LIST_ABI, MARKS_LIST_ADDRESS)
    //Keep the lists in the current state
    this.setState({ studentList })
    this.setState({ subjectList })
    this.setState({ marksList })
    //Get the number of records for all list in the blockchain
    const studentCount = await studentList.methods.studentsCount().call()
    const subjectCount = await subjectList.methods.subjectsCount().call()
    const marksCount = await marksList.methods.marksCount().call()
    //Store this value in the current state as well
    this.setState({ studentCount })
    this.setState({ subjectCount })
    this.setState({ marksCount })
    //Use an iteration to extract each record info and store
    //them in an array
    this.state.students = []
    for (var i = 1; i <= studentCount; i++) {
      const student = await studentList.methods.students(i).call()
      this.setState({
        students: [...this.state.students, student]
      })
    }
    this.state.subjects = []
    for (i = 1; i <= subjectCount; i++) {
      const subject = await subjectList.methods.subjects(i).call()
      this.setState({
        subjects: [...this.state.subjects, subject]
      })
    }
  }
  //Initialise the variables stored in the state
  constructor(props) {
    super(props)
    this.state = {
      account: '',
      studentCount: 0,
      students: [],
      subjectCount: 0,
      subjects: [],
      marksCount: 0,
      marks: [],
      loading: true,
    }
    this.createStudent = this.createStudent.bind(this)
    this.markGraduated = this.markGraduated.bind(this)
    this.createSubject = this.createSubject.bind(this)
    this.markRetired = this.markRetired.bind(this)
    this.createMarks = this.createMarks.bind(this)
    this.findMarks = this.findMarks.bind(this)
  }
  createStudent(cid, name) {
    this.setState({ loading: true })
    this.state.studentList.methods.createStudent(cid, name)
      .send({ from: this.state.account })
      .once('receipt', (receipt) => {
        this.setState({ loading: false })
        this.loadBlockchainData()
      })
  }
  markGraduated(cid) {
    this.setState({ loading: true })
    this.state.studentList.methods.markGraduated(cid)
      .send({ from: this.state.account })
      .once('receipt', (receipt) => {
        this.setState({ loading: false })
        this.loadBlockchainData()
      })
  }
  createSubject(id, name) {
    this.setState({ loading: true })
    this.state.subjectList.methods.createSubject(id, name).send({ from: this.state.account })
      .once('receipt', (receipt) => {
        this.setState({ loading: false })
        this.loadBlockchainData()
      })
  }
  markRetired(id) {
    this.setState({ loading: true })
    this.state.subjectList.methods.markRetired(id).send({ from: this.state.account })
      .once('receipt', (receipt) => {
        this.setState({ loading: false })
        this.loadBlockchainData()
      })
  }
  createMarks(studentid, subjectid, grades) {
    this.setState({ loading: true })
    this.state.marksList.methods.addMarks(studentid, subjectid, grades)
      .send({ from: this.state.account })
      .once('receipt', (receipt) => {
        this.setState({ loading: false })
        this.loadBlockchainData()
      })
  }
  findMarks(studentid,subjectid) {
    this.setState({ loading: true })
    return this.state.marksList.methods
      .findMarks(studentid,subjectid)
      .call({ from: this.state.account })
  }
  //Display the account and student list
  render() {
    return (
      <div className="container">
        <h1>Student Marks Management System</h1>
        <p>Your account: {this.state.account}</p>
        <p />
        <StudentList
          createStudent={this.createStudent} />
        <ul id="studentList" className="list-unstyled">
          {
            //This gets the each student from the studentList
            //and pass them into a function that display the 
            //details of the student
            this.state.students.map((student, key) => {
              return (
                <li className="list-group-item checkbox" key={key}>
                  <span className="name alert">{student._id}. {student.cid} {student.name}</span>
                  <input
                    className="form-check-input"
                    type="checkbox"
                    name={student._id}
                    defaultChecked={student.graduated}
                    disabled={student.graduated}
                    ref={(input) => {
                      this.checkbox = input
                    }}
                    onClick={(event) => {
                      this.markGraduated(event.currentTarget.name)
                    }} />
                  <label className="form-check-label" >Graduated</label>
                </li>
              )
            }
            )
          }
        </ul>
        <SubjectList
          createSubject={this.createSubject} />
        <p />
        <ul id="subjectList" className="list-unstyled">
          {
            //This gets the each subject from the subjectList
            //and pass them into a function that display the 
            //details of the subject
            this.state.subjects.map((subject, key) => {
              return (
                <li className="list-group-item checkbox" key={key}>
                  <span className="name alert">{subject._id}. {subject.code} {subject.name}</span>
                  <input
                    className="form-check-input"
                    type="checkbox"
                    name={subject._id}
                    defaultChecked={subject.retired}
                    disabled={subject.retired}
                    ref={(input) => {
                      this.checkbox = input
                    }}
                    onClick={(event) => {
                      this.markRetired(event.currentTarget.name)
                    }} />
                  <label className="form-check-label" >Retired</label>
                </li>
              )
            }
            )
          }
        </ul>
        <MarksList
          subjects={this.state.subjects}
          students={this.state.students}
          createMarks={this.createMarks}
        />
        <p>Total marks records : {this.state.marksCount}</p>
        <FindMarks
        subjects = {this.state.subjects}
        students = {this.state.students}
        findMarks={this.findMarks}  
        />
      </div>
    );
  }

}
export default App;