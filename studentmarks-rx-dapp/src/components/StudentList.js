import React, { Component } from 'react'

class StudentList extends Component {
  render() {
    return (
      <form className="p-5 border" onSubmit={(event) => {
        event.preventDefault()
        this.props.createStudent(this.cid.value, this.student.value)
      }}>
        <h2>Add Student</h2>
        <input id="newCID" type="text"
          ref={(input) => { this.cid = input; }}
          className="form-control m-1"
          placeholder="CID"
          required
        />
        <input id="newStudent" type="text"
          ref={(input) => { this.student = input; }}
          className="form-control m-1"
          placeholder="Student Name"
          required
        />
        <input className="form-control btn-primary" type="submit" hidden="" />
      </form>
    );
  }
}
export default StudentList;