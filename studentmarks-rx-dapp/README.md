# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

node ➜ /workspace (main ✗) $ truffle migrate --network ropsten

Compiling your contracts...
===========================
> Everything is up to date, there is nothing to compile.



Starting migrations...
======================
> Network name:    'ropsten'
> Network id:      3
> Block gas limit: 30000000 (0x1c9c380)


1_initial_migration.js
======================

   Deploying 'Migrations'
   ----------------------
   > transaction hash:    0xfc4f4af31db62b4411c6dc4899f3174a7baf8f14f347003e61074139e7366c31
   > Blocks: 4            Seconds: 21
   > contract address:    0x010dF826A26A434CC6858EadAdeA7edC6068889a
   > block number:        12362379
   > block timestamp:     1654847676
   > account:             0x178b8Cca56f7e613C525dBe1772e6F6a10Ee95Db
   > balance:             29.878641879092201035
   > gas used:            245600 (0x3bf60)
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.004912 ETH

   Pausing for 2 confirmations...
   ------------------------------
   > confirmation number: 1 (block: 12362380)
   > confirmation number: 2 (block: 12362381)

   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:            0.004912 ETH


2_deploy_contracts.js
=====================

   Deploying 'StudentList'
   -----------------------
   > transaction hash:    0xdda2ea100eac12d37d8dbb4cec91a944ef2822652a91548640cb4ed970eadea8
   > Blocks: 2            Seconds: 17
   > contract address:    0x762Aaf3106011CF7728e3A137cc8ADb04562B187
   > block number:        12362385
   > block timestamp:     1654847748
   > account:             0x178b8Cca56f7e613C525dBe1772e6F6a10Ee95Db
   > balance:             29.863376359092201035
   > gas used:            717363 (0xaf233)
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.01434726 ETH

   Pausing for 2 confirmations...
   ------------------------------
   > confirmation number: 1 (block: 12362386)
   > confirmation number: 2 (block: 12362387)

   Deploying 'SubjectList'
   -----------------------
   > transaction hash:    0xbc7ae439993b3682afd46e9f46e54751ab87118ceff98855ed2a87812976d091
   > Blocks: 2            Seconds: 21
   > contract address:    0x0f19881bC937E38a952F4ebE9fE915664ED30035
   > block number:        12362389
   > block timestamp:     1654847796
   > account:             0x178b8Cca56f7e613C525dBe1772e6F6a10Ee95Db
   > balance:             29.845983919092201035
   > gas used:            869622 (0xd44f6)
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.01739244 ETH

   Pausing for 2 confirmations...
   ------------------------------
   > confirmation number: 1 (block: 12362390)
   > confirmation number: 2 (block: 12362391)

   Deploying 'MarksList'
   ---------------------
   > transaction hash:    0x099df9c4b47701f61044c750e4c8b6ed01882f22397bc3e63a5c6ca956942f7a
   > Blocks: 2            Seconds: 21
   > contract address:    0x9D43e1Eb06A9190666f9448D13CcaeB4eA8E799E
   > block number:        12362393
   > block timestamp:     1654847844
   > account:             0x178b8Cca56f7e613C525dBe1772e6F6a10Ee95Db
   > balance:             29.831626459092201035
   > gas used:            717873 (0xaf431)
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.01435746 ETH

   Pausing for 2 confirmations...
   ------------------------------
   > confirmation number: 1 (block: 12362395)
   > confirmation number: 2 (block: 12362396)

   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.04609716 ETH


Summary
=======
> Total deployments:   4
> Final cost:          0.05100916 ETH
