var StudentsList = artifacts.require("StudentList");
var SubjectsList = artifacts.require("SubjectList");
var MarksList = artifacts.require("MarksList");
module.exports = function(deployer) {
  deployer.deploy(StudentsList);
  deployer.deploy(SubjectsList);
  deployer.deploy(MarksList);
};