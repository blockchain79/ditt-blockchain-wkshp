#Use a small and efficient linux image for deployment
FROM mhart/alpine-node:12 AS builder
#Set the root of application folder to be app
WORKDIR /app
#Copy our React folder to the root of container
COPY studentmarks-rx-dapp .
#Build the React Application
RUN apk add --no-cache git openssh
RUN yarn install
RUN yarn
RUN yarn run build
#Copy the configuration for nginx web server to the
#configuration folder
FROM nginx:alpine
COPY ./nginx.conf /etc/nginx/conf.d/default.conf
#Start the web server
COPY --from=builder /app/build /usr/share/nginx/html
CMD sed -i -e 's/$PORT/'"$PORT"'/g' /etc/nginx/conf.d/default.conf && nginx -g 'daemon off;'